import React, {Component} from 'react';
import config from './config'
import axios from 'axios'
import socketIOClient from "socket.io-client";

const socket = socketIOClient(config.url);

class App extends Component {
  constructor() {
    super();
    this.state = {
      dish: '',
      quantity: '',
      orders: []
    };
    this.input = this.input.bind(this);
    this.placeOrder = this.placeOrder.bind(this);
    this.getOrderData = this.getOrderData.bind(this);
    this.done = this.done.bind(this);
  }

  /**
   * Updating inputs values in state
   * @param e onchange event
   */
  input(e) {
    if (e.target.id === 'dish') {
      this.setState({
        dish: e.target.value,
      })
    }
    else if (e.target.id === 'quantity') {
      this.setState({
        quantity: (parseInt(e.target.value) < 0 ? '' : e.target.value),
      })
    }
  }

  componentWillMount() {
    this.getOrderData();
  }

  componentDidMount() {
    var that = this;
    socket.on('update', function (data) {
      that.setState({
        orders: data
      });
    });
  }

  getOrderData() {
    var that = this;
    axios({
      method: 'get',
      url: config.url + 'getOrders',
      crossOrigin: true,
    })
        .then(function (response) {
          // console.log("ok received", response);
          if (response.data.status === 200) {
            that.setState({
              orders: response.data.data
            });
          }
        })
        .catch(function (error) {
          console.log(error);
        });
  }

  placeOrder() {
    if (this.state.dish === '' || this.state.quantity === '') {
      window.alert('Enter Dish name and quantity');
      return;
    }

    var that = this;
    axios({
      method: 'post',
      url: config.url + 'addOrder',
      crossOrigin: true,
      data: {
        dish: this.state.dish,
        quantity: this.state.quantity,
        whenAdded: new Date()
      }
    })
        .then(function (response) {
          // console.log("ok received", response);
          that.setState({
            dish: '',
            quantity: '',
          });
        })
        .catch(function (error) {
          console.log(error);
        });
  }

  done(e) {
    axios({
      method: 'post',
      url: config.url + 'updateOrder',
      crossOrigin: true,
      data: {
        id: e.target.id,
        whenAdded: new Date()
      }
    })
        .then(function (response) {
          // console.log("ok received", response);
        })
        .catch(function (error) {
          console.log(error);
        });
  }


  download() {
    axios({
      method: 'get',
      url: config.url + 'download',
      crossOrigin: true,
    })
        .then(function (response) {
          // console.log("ok received", response);
          const a = document.createElement('a');
          a.href = config.url + 'Orders.csv';
          a.target = '_blank';
          a.download = 'Orders.csv';
          document.body.appendChild(a);
          a.click();
        })
        .catch(function (error) {
          console.log(error);
        });
  }


  render() {
    var that = this;
    let row = '';
    if (this.state.orders && this.state.orders.length === 0) {
      row = (
          <tr>
            <td colSpan="5" className='text-center'>We need orders...!!!</td>
          </tr>
      );
    }
    else {
      row = this.state.orders.map(function (item, index) {
        return (
            <tr key={index}>
              <td>{item.Dish}</td>
              <td>{item.Quantity}</td>
              <td>{item.Created}</td>
              <td>{item.Predicted}</td>
              <td>
                <button id={item._id} className='btn btn-default' onClick={that.done}>Done</button>
              </td>
            </tr>
        );
      })
    }

    return (
        <div>
          <div className='text-center'>
            <h1>Faasos Assignment</h1>
            <br/>
          </div>
          <div className='container-fluid'>
            <div className='row text-center'>
              <div className='col-lg-3 form-group'>
                <input type='text'
                       value={this.state.dish}
                       onChange={this.input}
                       placeholder='Enter dish'
                       className='form-control'
                       id='dish'/>
              </div>
              <div className='col-lg-3 form-group'>
                <input type='number'
                       value={this.state.quantity}
                       onChange={this.input}
                       placeholder='Quantity'
                       className='form-control'
                       id='quantity'/>
              </div>
              <div className='col-lg-1'>
                <button className='btn btn-default' onClick={this.placeOrder}>Add</button>
              </div>

              <div className='col-lg-5 text-right'>
                <button className='btn btn-default' onClick={this.download}>Download</button>
              </div>

            </div>
            <div className='row'>
              <div className='col-lg-12'>
                <table className='table table-bordered'>
                  <thead className='text-center'>
                  <tr>
                    <th>Dish</th>
                    <th>Quantity</th>
                    <th>Created Till Now</th>
                    <th>Predicted</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  {row}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default App;
